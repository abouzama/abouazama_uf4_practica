package es.almata.abouazama.placabase.connectables;



import es.almata.abouazama.placabase.instalables.PlacaBase;


public class Computadora {
	public static final String INFO_EXISTEIX_UNA_PLACA_BASE_A_LA_COMPUTADORA = "ya existeix una Placa Base a la Computadora";
	public static final String INFO_AQUESTA_PLACA_L_HAS_INSTALAT_ABANS = "Aquesta Placa l'has instalat abans..";
	public static final String INFO_FALTA_ALGUN_COMPONENT_A_LA_PLACA_BASE = "Falta algun component a la placa base";
	public static final String INFO_PLACA_BASE_INSTALAT_CORRECTAMENT = "Placa base instalat correctament..";
	public static final String INFO_INSTAL_LEM_LA_PLACA_BASE_A_LA_COMPUTADORA = "instal.lem la placa base a la computadora..";
	public static final String INFO_CONECTEM_UN_RATOLI_A_LA_COMPUTADORA = "conectem un Ratoli a la computadora";
	public static final String INFO_CONECTEM_UN_DVD_A_LA_COMPUTADORA = "conectem un DVD a la computadora";
	private static final String INFO_CONECTEM_UN_TECLAT_A_LA_COMPUTADORA = "conectem Teclat a la computadora";
	private static final String INFO_CONECTEM_UN_MONITOR_A_LA_COMPUTADORA = "conectem Monitor a la  computadora";
	private static final String INFO_CONECTEM_UN_FONT_A_LA_COMPUTADORA = "conectem Font d'alimentacio a la computadora";
	private static final String INFO_CONECTEM_UN_DISC_A_LA_COMPUTADORA = "conectem DISC DUR a la  computadora";
	private static final String PREPAREM_LA_COMPUTADORA = "Preparem la Computadora";
	public static final String NO_TENS_PLACA_BASE_A_LA_COMPUTADORA = "NO tens Placa Base a la computadora!!";
	public static final String DISPOSITIU_NO_ES_CONNECTABLE = "Dispositiu No es Connectable";
	public static final String LA_TAPA_NO_ESTA_OBERTA = "No pods conectar dispositiu por que la tapa no esta oberta";
	public static final String AQUEST_DISPOSITIU_JA_ESTÀ_CONNECTAT = "Aquest dispositiu ja està connecta !";
	public static final String _DISPOSITIU_IS_NULL = "No pots connectar un dispositiu que no tens!->(DISPOSITIU IS NULL)";
	public static final String HI_HA_UN_DISPOSITIU = "hi ha un dispositiu a la computadora no pods posar un altre !!";
	public static final String DISPOSITIU_CONNECTAT_CORRECTAMENT = "Dispositiu connectat correctament !!";

	
	
	
	
	private String id;
	private boolean connectorTeclatUSB;
	private boolean connectorRatolitUSB;
	private boolean connectorHDSata;
	private boolean connectorDVDPata;
	private boolean connectorFont24Pins;
	private boolean connectorMonitorHDMI;
	private String badies_5_1_4 = "Badia 5 1/4";
	private String badies_3_1_2 = "Badia 3 1/2";
	private String espaiFont ="ATX";
	
	private boolean tapaoberta;
	
	
	// atribut fruit de la relacio
	private PlacaBase placaBase = null;
	private DVD dvd = null;
	private Ratoli ratoli = null;
	private Teclat teclat = null;
	private Monitor monitor = null;
	private FontAlimentacio Fontalimentacio = null;
	private DiscDur discdur = null;
	//CONSTRUCTORS 
	
	public Computadora(String id, boolean connectorTeclatUSB, boolean connectorRatolitUSB, boolean connectorHDSata,
			boolean connectorDVDPata, boolean connectorFont24Pins, boolean connectorMonitorHDMI) {
		System.out.println(PREPAREM_LA_COMPUTADORA);
		this.id = id;
		this.connectorTeclatUSB = connectorTeclatUSB;
		this.connectorRatolitUSB = connectorRatolitUSB;
		this.connectorHDSata = connectorHDSata;
		this.connectorDVDPata = connectorDVDPata;
		this.connectorFont24Pins = connectorFont24Pins;
		this.connectorMonitorHDMI = connectorMonitorHDMI;
		
	}
	
	public Computadora(String id) {
		System.out.println(PREPAREM_LA_COMPUTADORA);
		this.id = id;
	}

	public Computadora() {
		System.out.println(PREPAREM_LA_COMPUTADORA);
	}
	
	// comportaments
	public boolean iniciarEquip() {
		boolean inici = false;
		System.out.println("Iniciem la computadora...");
		if(testDispositius()) {
			System.out.println("Computadora iniciada correctament!");
			inici = true;
		}else {
			System.out.println("Falta Algun component!");
		}

		return inici;
	}
	
	public void instalarPlacaBase(PlacaBase placaBase) {
		System.out.println(INFO_INSTAL_LEM_LA_PLACA_BASE_A_LA_COMPUTADORA);
		if(this.placaBase == null) {
			if(placaBase.testconexions()) {
				this.placaBase = placaBase;
				System.out.println(INFO_PLACA_BASE_INSTALAT_CORRECTAMENT);
			}else System.out.println(INFO_FALTA_ALGUN_COMPONENT_A_LA_PLACA_BASE);
		}else if(this.placaBase.equals(placaBase)) {
			System.out.println(INFO_AQUESTA_PLACA_L_HAS_INSTALAT_ABANS);
		}else if(this.placaBase != null)System.out.println(INFO_EXISTEIX_UNA_PLACA_BASE_A_LA_COMPUTADORA);
	}

	public boolean testDispositius() {
		boolean test = false;
		// Aquest condicional per assegurar que esta tot perfectament connectat 
		// por que si tot esta conectat siginfica que compleiexn els condicions 
		if (placaBase!= null && dvd != null && ratoli != null 
			&& teclat != null && monitor != null && Fontalimentacio != null && discdur != null ) {
			test = true;
		}
		return test;
	}

	public int connectarDispositius(ConnectablePC connectablepc) {
		int error = 0;
		if(this.placaBase != null) {
			if (connectablepc instanceof DVD) {
				System.out.println(INFO_CONECTEM_UN_DVD_A_LA_COMPUTADORA);
				error = connectarDVD((DVD) connectablepc);				// dvd
			}else if(connectablepc instanceof Ratoli){
				System.out.println(INFO_CONECTEM_UN_RATOLI_A_LA_COMPUTADORA);
				error = connectarRatoli((Ratoli) connectablepc);
			}else if(connectablepc instanceof Teclat) {
				System.out.println(INFO_CONECTEM_UN_TECLAT_A_LA_COMPUTADORA);
				error = connectarTeclat((Teclat) connectablepc);
			}else if(connectablepc instanceof Monitor) {
				System.out.println(INFO_CONECTEM_UN_MONITOR_A_LA_COMPUTADORA);
				error = connectarMonitor((Monitor) connectablepc);
			}else if(connectablepc instanceof FontAlimentacio) {
				System.out.println(INFO_CONECTEM_UN_FONT_A_LA_COMPUTADORA);
				error = connectarFontAlimentacio((FontAlimentacio) connectablepc);
			}else if(connectablepc instanceof DiscDur) {
				System.out.println(INFO_CONECTEM_UN_DISC_A_LA_COMPUTADORA);
				error = connectarDscDur((DiscDur) connectablepc);
			}else if (connectablepc == null) {
				error = 2; 
			}
		}else error = 6;
		
		return error;
	}
	// Polimorfisme de is connectable per tots els instalbles
	public boolean isconnectable(ConnectablePC connectablepc) {
		boolean inst = false;
		if (connectablepc.isConnectable(placaBase.obtenirXipSet(),this) == 0) {
			inst = true;
		}
		return inst;
	}

	/**returns
	 * 0-> NO HI HACAP ERROR DIPOSITIU INSTALAT CORRECTAMENT
	 * 1->HI ha UN DISPOSITIU A LA COMPUTADORA NO PODS POSAR UN ALTRE 
	 * 2->DISPOSITIU IS NULL AQUEST ESTA CONTROLAT DESDE CONNECTAR DIPOSITIUS 
	 * 3-> EL MATEIX DISPOSITIU CONECTAT
	 * 4-> LA TAPA NO ESTA OBERTA 
	 */
	// metode de negoci amb el dvd
	private int connectarDVD(DVD dvd) {
		int error = 0;// n'hi ha cap error o conectat correctament
		if(isconnectable(dvd)) {
			if (isTapaoberta()) {
				if (this.dvd == null) {
					if (dvd != null) {
						this.dvd = dvd;
					} 
				} else {
					if (this.dvd != null) {
						error = 1;// hi ha un un dvd no pods eficar un altre
						if (dvd.equals(this.dvd)) {
							error = 4;// el mateix dispositiu per es el mateix adressa de memoria
						}
					}
				}
			} else {
				error = 4;// tapa esta tancada
			}
		}else error = 5;
		
			
		return error;
	}
	/**returns
	 * 0-> NO HI HACAP ERROR DIPOSITIU INSTALAT CORRECTAMENT
	 * 1->HI ha UN DISPOSITIU A LA COMPUTADORA NO PODS POSAR UN ALTRE 
	 * 2->DISPOSITIU IS NULL AQUEST ESTA CONTROLAT DESDE CONNECTAR DIPOSITIUS 
	 * 3-> EL MATEIX DISPOSITIU CONECTAT
	 * 4-> LA TAPA NO ESTA OBERTA 
	 */
	// metode de negoci amb el RATOLI
	private int connectarRatoli(Ratoli ratoli) {
		int error = 0;// n'hi ha cap error o conectat correctament
		if(isconnectable(ratoli)) {
			
				if (this.ratoli == null) {
					if (ratoli != null) {
						this.ratoli = ratoli;
					} 
				} else {
					if (this.ratoli != null) {
						error = 1;// hi ha un ratoli no pods eficar un altre
						if (ratoli.equals(this.ratoli)) {
							error = 4;// el mateix dispositiu
						}
					}
				}
			
		}else error = 5;
		
			
		return error;
	}
	/**returns
	 * 0-> NO HI HACAP ERROR DIPOSITIU INSTALAT CORRECTAMENT
	 * 1->HI ha UN DISPOSITIU A LA COMPUTADORA NO PODS POSAR UN ALTRE 
	 * 2->DISPOSITIU IS NULL AQUEST ESTA CONTROLAT DESDE CONNECTAR DIPOSITIUS 
	 * 3-> EL MATEIX DISPOSITIU CONECTAT
	 * 4-> LA TAPA NO ESTA OBERTA 
	 */
	// metode de negoci amb el Teclat
	private int connectarTeclat(Teclat teclat) {
		int error = 0;// n'hi ha cap error o conectat correctament
		if(isconnectable(teclat)) {
			
				if (this.teclat == null) {
					if (teclat != null) {
						this.teclat = teclat;
					} 
				} else {
					if (this.teclat != null) {
						error = 1;// hi ha unteclat no pods eficar un altre
						if (teclat.equals(this.teclat)) {// adr d mmemoria
							error = 4;// el mateix dispositiu
						}
					}
				}
			
		}else error = 5;
		
			
		return error;
	}
	/**returns
	 * 0-> NO HI HACAP ERROR DIPOSITIU INSTALAT CORRECTAMENT
	 * 1->HI ha UN DISPOSITIU A LA COMPUTADORA NO PODS POSAR UN ALTRE 
	 * 2->DISPOSITIU IS NULL AQUEST ESTA CONTROLAT DESDE CONNECTAR DIPOSITIUS 
	 * 3-> EL MATEIX DISPOSITIU CONECTAT
	 * 4-> LA TAPA NO ESTA OBERTA 
	 */
	// metode de negoci amb el MONITOR
	private int connectarMonitor(Monitor monitor) {
		int error = 0;// n'hi ha cap error o conectat correctament
		if(isconnectable(monitor)) {
			
				if (this.monitor == null) {
					if (monitor != null) {
						this.monitor = monitor;
					} 
				} else {
					if (this.monitor != null) {
						error = 1;// hi ha un un monitor no pods eficar un altre
						if (monitor.equals(this.monitor)) {// adr de memoria
							error = 4;// el mateix dispositiu
						}
					}
				}
			
		}else error = 5;
		
			
		return error;
	}
	/**returns
	 * 0-> NO HI HACAP ERROR DIPOSITIU INSTALAT CORRECTAMENT
	 * 1->HI ha UN DISPOSITIU A LA COMPUTADORA NO PODS POSAR UN ALTRE 
	 * 2->DISPOSITIU IS NULL AQUEST ESTA CONTROLAT DESDE CONNECTAR DIPOSITIUS 
	 * 3-> EL MATEIX DISPOSITIU CONECTAT
	 * 4-> LA TAPA NO ESTA OBERTA 
	 */
	// metode de negoci amb el FONT
	private int connectarFontAlimentacio(FontAlimentacio Font) {
		int error = 0;// n'hi ha cap error o conectat correctament
		if(isconnectable(Font)) {
			if (isTapaoberta()) {
				if (this.Fontalimentacio == null) {
					if (Font != null) {
						this.Fontalimentacio = Font;
					} 
				} else {
					if (this.Fontalimentacio != null) {
						error = 1;// hi ha un un Font no pods eficar un altre
						if (Font.equals(this.Fontalimentacio)) {// adr de memoria
							error = 4;// el mateix dispositiu
						}
					}
				}
			} else {
				error = 4;// tapa esta tancada
			}
		}else error = 5;
		
			
		return error;
	}
	/**returns
	 * 0-> NO HI HACAP ERROR DIPOSITIU INSTALAT CORRECTAMENT
	 * 1->HI ha UN DISPOSITIU A LA COMPUTADORA NO PODS POSAR UN ALTRE 
	 * 2->DISPOSITIU IS NULL AQUEST ESTA CONTROLAT DESDE CONNECTAR DIPOSITIUS 
	 * 3-> EL MATEIX DISPOSITIU CONECTAT
	 * 4-> LA TAPA NO ESTA OBERTA 
	 */
	// metode de negoci amb el DISC DUR
	private int connectarDscDur(DiscDur Disc) {
		int error = 0;// n'hi ha cap error o conectat correctament
		if(isconnectable(Disc)) {
			if (isTapaoberta()) {
				if (this.discdur == null) {
					if (Disc != null) {
						this.discdur = Disc;
					} 
				} else {
					if (this.discdur != null) {
						error = 1;// hi ha un un DIsc no pods eficar un altre
						if (Disc.equals(this.discdur)) {// adr de memoria
							error = 4;// el mateix dispositiu
						}
					}
				}
			} else {
				error = 4;// tapa esta tancada
			}
		}else error = 5;
		
			
		return error;
	}
	// aqui es controla els errors que poden sld
	public void infoerrors(int errors) {
		switch (errors) {
		case 0:
			System.out.println(DISPOSITIU_CONNECTAT_CORRECTAMENT);
			break;
		case 1:
			System.out.println(HI_HA_UN_DISPOSITIU);
			break;
		case 2:
			System.out.println(_DISPOSITIU_IS_NULL);
			break;
		case 3:
			System.out.println(AQUEST_DISPOSITIU_JA_ESTÀ_CONNECTAT);
			break;
		case 4:
			System.out.println(LA_TAPA_NO_ESTA_OBERTA);
			break;
		case 5:
			System.out.println(DISPOSITIU_NO_ES_CONNECTABLE);
			break;
		case 6:
			System.out.println(NO_TENS_PLACA_BASE_A_LA_COMPUTADORA);
			break;
		}
	}
	// getters & setters 
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public boolean isConnectorTeclatUSB() {
		return connectorTeclatUSB;
	}
	public void setConnectorTeclatUSB(boolean connectorTeclatUSB) {
		this.connectorTeclatUSB = connectorTeclatUSB;
	}
	public boolean isConnectorRatolitUSB() {
		return connectorRatolitUSB;
	}
	public void setConnectorRatolitUSB(boolean connectorRatolitUSB) {
		this.connectorRatolitUSB = connectorRatolitUSB;
	}
	public boolean isConnectorHDSata() {
		return connectorHDSata;
	}
	public void setConnectorHDSata(boolean connectorHDSata) {
		this.connectorHDSata = connectorHDSata;
	}
	public boolean isConnectorDVDPata() {
		return connectorDVDPata;
	}
	public void setConnectorDVDPata(boolean connectorDVDPata) {
		this.connectorDVDPata = connectorDVDPata;
	}
	public boolean isConnectorFont24Pins() {
		return connectorFont24Pins;
	}
	public void setConnectorFont24Pins(boolean connectorFont24Pins) {
		this.connectorFont24Pins = connectorFont24Pins;
	}
	public boolean isConnectorMonitorHDMI() {
		return connectorMonitorHDMI;
	}
	public void setConnectorMonitorHDMI(boolean connectorMonitorHDMI) {
		this.connectorMonitorHDMI = connectorMonitorHDMI;
	}
	public boolean isTapaoberta() {
		return tapaoberta;
	}
	public void setTapaoberta(boolean tapaoberta) {
		this.tapaoberta = tapaoberta;
	}
	public String getBadies_5_1_4() {
		return badies_5_1_4;
	}
	public String getBadies_3_1_2() {
		return badies_3_1_2;
	}
	public String getEspaiFont() {
		return espaiFont;
	}
}
