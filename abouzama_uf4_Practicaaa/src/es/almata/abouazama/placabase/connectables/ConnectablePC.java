package es.almata.abouazama.placabase.connectables;

import es.almata.abouazama.placabase.instalables.PlacaBase.XipSet;

public interface ConnectablePC {
	public int isConnectable(XipSet xipset,Computadora computadora);

}
