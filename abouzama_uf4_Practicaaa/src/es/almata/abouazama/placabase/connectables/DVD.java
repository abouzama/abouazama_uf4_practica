package es.almata.abouazama.placabase.connectables;

import es.almata.abouazama.placabase.instalables.PlacaBase.XipSet;

public class DVD implements ConnectablePC {
	private String velocitatEscriptura;
	private String velocitatLectura;
	private String connector;
	private String id;
	private String espaiDvd;

	
	@Override
	public int isConnectable(XipSet xipset,Computadora computadora) {
		int compatible=1;//no es compatible
		if((connector.equals(xipset.getConnectorDVD()) && computadora.isConnectorDVDPata()) && espaiDvd.equals(computadora.getBadies_5_1_4())) {
			compatible=0;//n'hi ha cap error
		}
			return compatible;
	}

	//Constructors
	public DVD() {
		
	}
	
	public DVD(String velocitatEscriptura, String velocitatLectura, String connector, String id, String espaiDvd) {
		
		this.velocitatEscriptura = velocitatEscriptura;
		this.velocitatLectura = velocitatLectura;
		this.connector = connector;
		this.id = id;
		this.espaiDvd = espaiDvd;
	}

	public DVD(String id) {
		
		this.id = id;
	}
	public String getVelocitatEscriptura() {
		return velocitatEscriptura;
	}
	
	public void setVelocitatEscriptura(String velocitatEscriptura) {
		this.velocitatEscriptura = velocitatEscriptura;
	}
	public String getVelocitatLectura() {
		return velocitatLectura;
	}
	public void setVelocitatLectura(String velocitatLectura) {
		this.velocitatLectura = velocitatLectura;
	}
	public String getConnector() {
		return connector;
	}
	public void setConnector(String connector) {
		this.connector = connector;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getEspaiDvd() {
		return espaiDvd;
	}

	public void setEspaiDvd(String espaiDvd) {
		this.espaiDvd = espaiDvd;
	}
	
	
	
		
}
