package es.almata.abouazama.placabase.connectables;

import es.almata.abouazama.placabase.instalables.PlacaBase.XipSet;

public class DiscDur implements ConnectablePC{
	private double capacita;
	private String connector;
	private String id;
	private String espaiDiscDur;
	//Constructors 
	public DiscDur(double capacita, String connector, String id,String espaiDiscDur) {
		this.capacita = capacita;
		this.connector = connector;
		this.id = id;
		this.espaiDiscDur = espaiDiscDur;
	}
	public DiscDur() {
	}
	// Metodes 
	@Override
	public int isConnectable(XipSet xipset, Computadora computadora) {
		int compatible=1;//no es compatible
		if(connector.equals(xipset.getConnectorDiscDur()) && espaiDiscDur.equals(computadora.getBadies_3_1_2())) {
			compatible=0;//n'hi ha cap error
		}
			return compatible;
	}
	// getters & setters 
	public double getCapacita() {
		return capacita;
	}
	public void setCapacita(double capacita) {
		this.capacita = capacita;
	}
	public String getConnector() {
		return connector;
	}
	public void setConnector(String connector) {
		this.connector = connector;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getEspaiDiscDur() {
		return espaiDiscDur;
	}
	public void setEspaiDiscDur(String espaiDiscDur) {
		this.espaiDiscDur = espaiDiscDur;
	}
	
	
	
	
	

}
