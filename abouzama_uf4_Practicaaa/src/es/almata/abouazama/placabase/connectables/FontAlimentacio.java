package es.almata.abouazama.placabase.connectables;

import es.almata.abouazama.placabase.instalables.PlacaBase.XipSet;

public class FontAlimentacio implements ConnectablePC{
	private int potencia;
	private String connector;
	private String id;
	private String espaiFont;
	// Constructors
	public FontAlimentacio(int potencia, String connector, String id,String espaiFont) {
		this.potencia = potencia;
		this.connector = connector;
		this.id = id;
		this.espaiFont = espaiFont;
	}
	public FontAlimentacio() {
		
	}
	// Metodes 
	@Override
	public int isConnectable(XipSet xipset, Computadora computadora) {
		
			int compatible=1;//no es compatible
			if((connector.equals(xipset.getConnectorFont()) 
				&& computadora.isConnectorFont24Pins())
				&& espaiFont.equals(computadora.getEspaiFont())) {
				compatible=0;//n'hi ha cap error
			}
				return compatible;
		
	}
	// getters & setters 
	public int getPotencia() {
		return potencia;
	}
	public void setPotencia(int potencia) {
		this.potencia = potencia;
	}
	public String getConnector() {
		return connector;
	}
	public void setConnector(String connector) {
		this.connector = connector;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
	
	

}