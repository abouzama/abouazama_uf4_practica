package es.almata.abouazama.placabase.connectables;

import es.almata.abouazama.placabase.instalables.PlacaBase.XipSet;

public class Monitor implements ConnectablePC  {
	private double frequencia;
	private double polsades;
	private String connector;
	private String id;
	
	
	// Constructors 
	public Monitor(double frequencia, double polsades, String connector, String id) {
		this.frequencia = frequencia;
		this.polsades = polsades;
		this.connector = connector;
		this.id = id;
	}
	public Monitor() {
	}
	//Metodes 
	@Override
	public int isConnectable(XipSet xipset, Computadora computadora) {
		int compatible=1;//no es compatible
		if((connector.equals(xipset.getConnectorMonitor()) && computadora.isConnectorMonitorHDMI())) {
			compatible=0;//n'hi ha cap error
		}
			return compatible;
	}
	// getters & setters 
	public double getFrequencia() {
		return frequencia;
	}
	public void setFrequencia(double frequencia) {
		this.frequencia = frequencia;
	}
	public double getPolsades() {
		return polsades;
	}
	public void setPolsades(double polsades) {
		this.polsades = polsades;
	}
	public String getConnector() {
		return connector;
	}
	public void setConnector(String connector) {
		this.connector = connector;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	
	

}
