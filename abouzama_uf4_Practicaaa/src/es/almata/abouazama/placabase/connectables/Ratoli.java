
package es.almata.abouazama.placabase.connectables;

import es.almata.abouazama.placabase.instalables.PlacaBase.XipSet;

public class Ratoli implements ConnectablePC {
	//Atribugts del Ratoli
	private String marca;
	private String connector;
	private String id;
	// Constructors	
	public Ratoli() {
	}
	public Ratoli(String marca, String connector, String id) {
	
		this.marca = marca;
		this.connector = connector;
		this.id = id;
	}
	public Ratoli(String id) {
		this.id = id;
	}
	// Metodes 
	@Override
	public int isConnectable(XipSet xipset, Computadora computadora) {
		int compatible=1;//no es compatible
		if((connector.equals(xipset.getConnectorRatoli()) && computadora.isConnectorRatolitUSB())) {
			compatible=0;//n'hi ha cap error
		}
		return compatible;
	}
	//getters & setters 
	
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getConnector() {
		return connector;
	}
	public void setConnector(String connector) {
		this.connector = connector;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	
	
	

}
