package es.almata.abouazama.placabase.connectables;

import es.almata.abouazama.placabase.instalables.PlacaBase.XipSet;

public class Teclat implements ConnectablePC{
	private String marca;
	private String tecnologia;
	private String id;
	private String connector;
	// COnstructors
	public Teclat() {	
	}
	public Teclat(String marca, String tecnologia, String id, String connector) {
		this.marca = marca;
		this.tecnologia = tecnologia;
		this.id = id;
		this.connector = connector;
	}
	// Metodes 
	@Override
	public int isConnectable(XipSet xipset,Computadora computadora) {
		int compatible=1;//no es compatible
		if((connector.equals(xipset.getConnectorTeclat()) && computadora.isConnectorTeclatUSB())) {
			compatible=0;//n'hi ha cap error
		}
			return compatible;
	}

	// getters & setters 
	
	public String getMarca() {
		return marca;
	}
	public String getConnector() {
		return connector;
	}
	public void setConnector(String connector) {
		this.connector = connector;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getTecnologia() {
		return tecnologia;
	}
	public void setTecnologia(String tecnologia) {
		this.tecnologia = tecnologia;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	
	
	
	
	

}
