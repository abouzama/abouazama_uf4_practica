package es.almata.abouazama.placabase.instalables;

public interface InstalablePB {
	public abstract int isInstalable(PlacaBase placabase);

}
