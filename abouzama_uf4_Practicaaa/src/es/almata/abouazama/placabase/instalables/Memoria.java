package es.almata.abouazama.placabase.instalables;



public class Memoria implements InstalablePB {
	private String frequencia;
	private String tipusMemoria;
	private String id;
	

	//Constructors
	public Memoria() {
	}
	public Memoria(String frequencia, String tipusRam, String id) {
		this.frequencia = frequencia;
		this.tipusMemoria = tipusRam;
		this.id = id;
	}
	public Memoria(String id) {
		this.id = id;
	}
	

	// AQuesta es per comprobar si la memoria is instalable o no
	@Override
	public int isInstalable(PlacaBase placabase) {
		int instalable = 1;
		if(frequencia.equals(placabase.obtenirXipSet().getFrequenciaMemoria()) && tipusMemoria.equals(placabase.obtenirXipSet().getTipusMemoria())) {
			instalable = 0;// en aquest no es instalble en la meva Placa Base
		}
		return instalable;
	}
	//getters & setters 
	// getters & setters de la frequencia 
	
	public String getFrequencia() {
		return frequencia;
	}
	public void setFrequencia(String frequencia) {
		this.frequencia = frequencia;
	}
	// getters & setters del Tipus Ram 
	public String getTipusRam() {
		return tipusMemoria;
	}
	public void setTipusRam(String tipusRam) {
		this.tipusMemoria = tipusRam;
	}
	// getters & setters del ID
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	


	
}
