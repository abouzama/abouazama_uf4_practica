package es.almata.abouazama.placabase.instalables;

public class Microprocessador implements InstalablePB {
	private double velocitat;
	private String tipusSocol;
	private String tipusProcessador;
	private String id;
	//constructors
	
	public Microprocessador() {
	}
	public Microprocessador(double velocitat, String tipusSocol, String tipusProcessador, String id) {
		this.velocitat = velocitat;
		this.tipusSocol = tipusSocol;
		this.tipusProcessador = tipusProcessador;
		this.id = id;
	}
	public Microprocessador(String id) {
		this.id = id;
	}
	//Metodes o comportaments 
	@Override
	public int isInstalable(PlacaBase placabase) {
		int instalable = 0;// si surt amb aquest valor es instalable 
		if(!tipusProcessador.equals(placabase.obtenirXipSet().getTipusProcessador()) 
			&& !tipusSocol.equals(placabase.obtenirXipSet().getsocolProcessador())) {
			instalable = 1;// en aquest no es instalble en la meva Placa Base
		}
		return instalable;
	}
	// getters & setters 
	public double getVelocitat() {
		return velocitat;
	}
	public void setVelocitat(double velocitat) {
		this.velocitat = velocitat;
	}
	public String getTipusSocol() {
		return tipusSocol;
	}
	public void setTipusSocol(String tipusSocol) {
		this.tipusSocol = tipusSocol;
	}
	public String getTipusProcessador() {
		return tipusProcessador;
	}
	public void setTipusProcessador(String tipusProcessador) {
		this.tipusProcessador = tipusProcessador;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	

}
