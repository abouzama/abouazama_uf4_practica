package es.almata.abouazama.placabase.instalables;

public class PlacaBase {

	

	private XipSet xipset = null;

	private int ranuresPCI;
	private String id;
	private InstalablePB memoria = null;
	private InstalablePB grafica = null;
	private InstalablePB microprocessador = null;

	// Constants dels warnings i dels informacions
	public static final String MISS_INFO_MODUL_INSTALLAT = "Mòdul instal·lat correctament!!";
	public static final String MISS_WARNINGS_MODUL_NULL = "No pots instal·lar un mòdul que no tens--> (Objecte Introduit es NULL)!!";
	public static final String MISS_INFO_MODUL_EXISTEIX = "Aquest mòdul ja està instal·lat!";
	public static final String MISS_WARNINGS_MODUL_NOCOMPATIBLE = "Mòdul no es instal.lable!";
	private static final String MISS_WARNINGS_HI_HA_MODUL = "no pods efficar un Modul per que hi ha un instal.lat..";
	private static final String MISS_WARNINGS_MODUL_NO_ES_INSTALLABLE = "Modul no es instal.lable...!!!";
	private static final String PREPAREM_LA_PLACABASE = "Preparem la Placabase...";
	// un constructor vuid per assegurar que no se instancia mes d'una vegada el
	// XipSet
	public PlacaBase() {
		System.out.println(PREPAREM_LA_PLACABASE);
		// insanciar el clase Xipset a la mateixa vegada que el placabase
		xipset = obtenirXipSet();
	}

	public PlacaBase(int ranuresPCI, String id) {
		this.ranuresPCI = ranuresPCI;
		this.id = id;
	}
	// metode per comprvar si tot instalat correctament
	public boolean testconexions() {
		boolean test = false;
		if(memoria != null && grafica != null
			&& microprocessador != null) {
			test = true ;// Aixo significa que tot instaat correctament per que per instal.lar cada cosa hi ha de passar al process i tot esta controlat
		}
		return test;
	}
	// aquest es el Mr instatlador de tots els Moduls a la Placa Base
	public int afegirModul(InstalablePB modul) {
		int error = 7; // si aquest variabe queda amb aquest valor significa que el obj es nuul i no es
						// instace of a ningu obj dels meu obj
		if (modul instanceof Memoria) {
			error = instalarMemoria((Memoria) modul);// en aquesta part esta controlan els error
			// i al mateixa veada instala la memoria si n'hi ha cap error
		} else if (modul instanceof TargetaGrafica) {
			error = instalarTargetaGrafica((TargetaGrafica) modul);
		}else if(modul instanceof Microprocessador) {
			error = instalarMicroProcessador((Microprocessador) modul);
		}
		return error;

	}

	// Metode de Negoci amb la memoria
	private int instalarMemoria(Memoria memoria) {
		int error = 0;// n'hi ha cap error
		System.out.println("Instal·lem la memòria RAM a la placa base...");
		if (isinstalable(memoria)) {
			// Aquest condiciona sirve per a que controlem que no hi ha cap memoria
			// instalada
			if (this.memoria == null) {
				// i aquest es controla per a que el obj que me arribe aqui no sigui null
				if (memoria != null) {
					// aqui es instal.lar la memoria
					this.memoria = memoria;
				}
			} else {
				// aquest condicional es Quan existeix una Memoria a placa i per aixo lo
				// controlem aquest erro
				if (this.memoria != null) {
					error = 1;// hi ha un una memoria no pods eficar un altre
					if (memoria.equals(this.memoria)) {// i aquest es si el usuari intenta a efica la mateixa memoria
						error = 4;// el mateix Modul
					}
				}
			}
		} else {
			error = 5;// aquest d'aqui es que el Modul no es Instal.lable
		}

		return error;
	}
	// Metode de Negoci amb la targeta grafica
	private int instalarTargetaGrafica(TargetaGrafica Grafica) {
		int error = 0;// n'hi ha cap error
		System.out.println("instal·lem la targeta Gràfica a la placa base..");
		if (isinstalable(Grafica)) {
			// Aquest condiciona sirve per a que controlem que no hi ha cap grafica
			// instalada
			if (this.grafica == null) {
				// i aquest es controla per a que el obj que me arribe aqui no sigui null
				if (Grafica != null) {
					// aqui es instal.lar la grafica
					this.grafica = Grafica;
				}
			} else {
				// aquest condicional es Quan existeix una grafica a placa i per aixo lo
				// controlem aquest erro
				if (this.grafica != null) {
					error = 1;// hi ha un una grafica no pods eficar un altre
					if (Grafica.equals(this.grafica)) {// i aquest es si el usuari intenta a efica la mateixa grafica
						error = 4;// el mateix Modul
					}
				}
			}
		} else {
			error = 5;// aquest d'aqui es que el Modul no es Instal.lable
		}

		return error;
	}
	/*returns
	 * 0-> n'hi cap eror
	 * 1->hi ha un modl insalat
	 * 4->el mateix Modul
	 * 5-> Modul no es instalable
	 * */
	// Metode de Negoci amb la MicroProcessador
		private int instalarMicroProcessador(Microprocessador micro) {
			int error = 0;// n'hi ha cap error
			System.out.println("instal·lem  Micro Processador a la placa base..");
			if (isinstalable(micro)) {
				// Aquest condiciona sirve per a que controlem que no hi ha cap grafica
				// instalada
				if (this.microprocessador == null) {
					// i aquest es controla per a que el obj que me arribe aqui no sigui null
					if (micro != null) {
						// aqui es instal.lar la grafica
						this.microprocessador = micro;
					}
				} else {
					// aquest condicional es Quan existeix una grafica a placa i per aixo lo
					// controlem aquest error
					if (this.microprocessador != null) {
						error = 1;// hi ha un una micro no pods eficar un altre
						if (micro.equals(this.microprocessador)) {// i aquest es si el usuari intenta a efica la mateixa grafica
							error = 4;// el mateix Modul
						}
					}
				}
			} else {
				error = 5;// aquest d'aqui es que el Modul no es Instal.lable
			}

			return error;
		}
	// Polimorfisme de is instalable per tots els instalbles
	public boolean isinstalable(InstalablePB instalable) {
		boolean inst = false;
		if (instalable.isInstalable(this) == 0) {
			inst = true;
		}
		return inst;
	}



	// (**)
	public XipSet obtenirXipSet() {
		// ***
		// Aquesta part es Controla per la class Xipse no se instancia una altra vegada
		if (xipset == null) {
			xipset = this.new XipSet();
		}
		return xipset;
	}

// aquesta metode es per obtenir el xipset i per accedir als metodes

// aquesa funnccio lo que fa es controla els errors que poden surtir quan instalem un Modul
	public void infoError(int errors) {
		switch (errors) {
		case 0:
			System.out.println(MISS_INFO_MODUL_INSTALLAT);
			break;
		case 1:
			System.out.println(MISS_WARNINGS_HI_HA_MODUL);
			break;

		case 4:
			System.out.println(MISS_INFO_MODUL_EXISTEIX);
			break;
		

		case 5:
			System.out.println(MISS_WARNINGS_MODUL_NO_ES_INSTALLABLE);
			break;
		case 7:
			System.out.println(MISS_WARNINGS_MODUL_NULL);
			break;
		}
	}

// getters and setters de placa Base 
	public int getRanuresPCI() {
		return ranuresPCI;
	}

	public void setRanuresPCI(int ranuresPCI) {
		this.ranuresPCI = ranuresPCI;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public class XipSet {
		private String socolProcessador;
		private String tipusProcessador;
		private String frequenciaMemoria;
		private String tipusBusGrafica;
		private String connectorDVD;
		private String connectorRatoli;
		private String connectorTeclat;
		private String tipusMemoria;
		private String connectorDiscDur;
		private String connectorMonitor;
		private String connectorFont;
		// *
		private XipSet() {
			socolProcessador = "LGA1366";
			frequenciaMemoria = "1333";
			tipusBusGrafica = "AGP";
			tipusProcessador = "Intel Core i7";
			connectorDVD = "Sata";
			connectorRatoli = "USB";
			connectorTeclat= "PS2";
			tipusMemoria="DDR4";
			connectorDiscDur="Sata";
			connectorMonitor="HDMI";
		    connectorFont="32 pins";
		}

		// metodes accesors
		// getters del xipset
		public String getFrequenciaMemoria() {
			return frequenciaMemoria;
		}


		public String getTipusBusGrafica() {
			return tipusBusGrafica;
		}

		public String getsocolProcessador() {
			return socolProcessador;
		}

		public String getTipusProcessador() {
			return tipusProcessador;
		}

		public String getConnectorDVD() {
			return connectorDVD;
		}

		public String getConnectorRatoli() {
			return connectorRatoli;
		}

		public String getConnectorTeclat() {
			return connectorTeclat;
		}

		public String getTipusMemoria() {
			return tipusMemoria;
		}

		public String getConnectorDiscDur() {
			return connectorDiscDur;
		}

		public String getConnectorMonitor() {
			return connectorMonitor;
		}

		public String getConnectorFont() {
			return connectorFont;
		}
		
		
		
		

	}
}
