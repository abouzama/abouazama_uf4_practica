package es.almata.abouazama.placabase.instalables;

public class TargetaGrafica implements InstalablePB{
	private String tipusBus;
	private String fabricant;
	private String resoluciomxima;
	private String id;
	
	// constructors 
	public TargetaGrafica(String tipusBus, String fabricant, String resoluciomxima, String id) {
		this.tipusBus = tipusBus;
		this.fabricant = fabricant;
		this.resoluciomxima = resoluciomxima;
		this.id = id;
	}
	public TargetaGrafica(String id) {
		this.id = id;
	}
	public TargetaGrafica() {
		
	}


	@Override
	public int isInstalable(PlacaBase placabase) {
		int instalable = 0;
		if(!tipusBus.equals(placabase.obtenirXipSet().getTipusBusGrafica())) {
			instalable = 1;// en aquest CAS no es instalble en la meva Placa Base
		}
		return instalable;
	}

	public String getTipusBus() {
		return tipusBus;
	}

	public void setTipusBus(String tipusBus) {
		this.tipusBus = tipusBus;
	}

	public String getFabricant() {
		return fabricant;
	}

	public void setFabricant(String fabricant) {
		this.fabricant = fabricant;
	}

	public String getResoluciomxima() {
		return resoluciomxima;
	}

	public void setResoluciomxima(String resoluciomxima) {
		this.resoluciomxima = resoluciomxima;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	

	

}
