package es.almata.abouzama;



import es.almata.abouazama.placabase.connectables.Computadora;
import es.almata.abouazama.placabase.connectables.DVD;
import es.almata.abouazama.placabase.connectables.DiscDur;
import es.almata.abouazama.placabase.connectables.FontAlimentacio;
import es.almata.abouazama.placabase.connectables.Monitor;
import es.almata.abouazama.placabase.connectables.Ratoli;
import es.almata.abouazama.placabase.connectables.Teclat;
import es.almata.abouazama.placabase.instalables.Memoria;
import es.almata.abouazama.placabase.instalables.Microprocessador;
import es.almata.abouazama.placabase.instalables.PlacaBase;
import es.almata.abouazama.placabase.instalables.TargetaGrafica;

public class Run {

	public static void main(String[] args) {
		
		//Primer configura al computadora i la placa base.
		Computadora computadora1 = new Computadora("C-1",true,true,true,true,true,true);
		
		PlacaBase placabase1 = new PlacaBase();
		placabase1.setId("PLACA-1");
		placabase1.setRanuresPCI(3);
		
		//Configura els instalables
		Memoria ram = new Memoria("1333", "DDR4", "RAM-1");
		
		TargetaGrafica tgrafica = new TargetaGrafica("AGP","Nividia","1212","TG-1");
		Microprocessador micro = new Microprocessador(12.0,"LGA1366","Intel Core i7","MP-1");
		System.out.print("\n");
		//instalem els instalables a la Placabase amb condiciones controlats
		placabase1.infoError(placabase1.afegirModul(ram));
		System.out.print("\n");
		placabase1.infoError(placabase1.afegirModul(tgrafica));
		System.out.print("\n");
		placabase1.infoError(placabase1.afegirModul(micro));
		//obrem la tapa de la computadora 
		computadora1.setTapaoberta(true);
		//Instal-lar la PlacaBase
		System.out.print("\n");
		computadora1.instalarPlacaBase(placabase1);
		
		//CONFIGURA ELS CONNECTABLES 
		DVD dvd = new DVD("12", "15", "Sata", "DVD-1","Badia 5 1/4");
		Ratoli ratolit= new Ratoli("Click", "USB", "RL-1");
		Monitor monitor = new Monitor(12.2, 22, "HDMI", "MONITOR-1");
		Teclat teclat = new Teclat("HP", "tec", "TEC-1", "PS2");
		DiscDur disc = new DiscDur(128, "Sata", "DISC-1", "Badia 3 1/2");
		FontAlimentacio font = new FontAlimentacio(232,"32 pins","FONT-1","ATX");
		
		//Connectem els connectable a la computadora amb condiciones controlats
		System.out.print("\n");
		computadora1.infoerrors(computadora1.connectarDispositius(dvd));
		System.out.print("\n");
		computadora1.infoerrors(computadora1.connectarDispositius(ratolit));
		System.out.print("\n");
		computadora1.infoerrors(computadora1.connectarDispositius(monitor));
		System.out.print("\n");
		computadora1.infoerrors(computadora1.connectarDispositius(teclat));
		System.out.print("\n");
		computadora1.infoerrors(computadora1.connectarDispositius(disc));
		System.out.print("\n");
		computadora1.infoerrors(computadora1.connectarDispositius(font));
		System.out.print("\n");
		// Iniciem La computadora
		computadora1.iniciarEquip();
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
